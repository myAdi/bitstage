from os import walk

def Show_dir(mypath):
    f = []
    d = []
    for (dirpath, dirnames, filenames) in walk(mypath):
        f.extend(filenames)
        d.extend(dirnames)
    return f, d


inPath = input('Enter path: ')
print(Show_dir(inPath))

